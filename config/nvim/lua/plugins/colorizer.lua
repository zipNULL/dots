return {
    "norcalli/nvim-colorizer.lua",
    config = function()
        require("colorizer").setup()
    end,
    keys = {
        { "<leader>c", "<cmd>ColorizerToggle<cr>", desc = "Colorizer" },
    },
}

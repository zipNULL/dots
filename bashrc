### BASHRC ###

## Exports
export MANPAGER="nvim +Man!"
export VISUAL='nvim'
export EDITOR='$VISUAL'
export HISTCONTROL=ignoredups:erasedups   # no duplicate entries
export TERM='kitty'
export PATH="${PATH}:$HOME/.local/bin"
# export wall="$(cat /home/null/.cache/swww/eDP-1)"
## For school
export ICAROOT="/home/null/Documents/ICAClient/linuxx64"

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


### Aliases
alias ls="eza -a --color=always --group-directories-first"
alias ll="eza -al --color=always --group-directories-first"
alias vi="nvim"
alias grep='grep --color=auto'
alias cp='cp -i'	#Confirm duplicates
alias mv='mv -i'	#Confirm duplicates
alias rm='rm -i'	#Confirm
alias mkdir='mkdir -pv' #Creates parents
alias ..='cd ..'
alias ...='cd ../..'

# git
alias addall='git add .'
alias commit='git commit -m'
alias push='git push'
alias pull='git pull'

#Kitty
alias icat="kitty +kitten icat"

#Pywall
#(cat ~/.cache/wal/sequences &)

colorbars

eval "$(starship init bash)"
